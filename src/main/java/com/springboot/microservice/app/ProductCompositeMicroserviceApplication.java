package com.springboot.microservice.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductCompositeMicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductCompositeMicroserviceApplication.class, args);
    }

}
