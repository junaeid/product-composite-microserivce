package com.springboot.microservice.review.app;

public class Review {
    private Long reviewId;
    private Long productId;
    private String author;
    private String subject;
    private String content;
    private String serviceAddress;
}
