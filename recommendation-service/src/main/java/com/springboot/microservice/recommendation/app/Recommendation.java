package com.springboot.microservice.recommendation.app;

public class Recommendation {
    private Long recommendationId;
    private Long productId;
    private String author;
    private int rate;
    private String content;
    private String serviceAddress;
}
