package com.springboot.microservice.product.app.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.springboot.microservice.product.app.model.dto.ProductDto;
import com.springboot.microservice.product.app.service.ProductServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.*;

@WebMvcTest(ProductController.class)
class ProductControllerTest {

    private static final String URI_PRODUCT_V1 = "/api/v1/product/";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    ObjectMapper objectMapper;
    @MockBean
    private ProductServiceImpl productService;

    ProductDto validProduct;

    @BeforeEach
    void setUp() {
        validProduct = ProductDto.builder()
                .name("product 1")
                .weight(3)
                .build();
    }

    @Test
    public void addNewProduct() throws Exception {
        //given
        validProduct = ProductDto.builder()
                .name("product 1")
                .weight(3)
                .build();
        given(productService.addProduct(any(ProductDto.class)))
                .willAnswer((invocation -> invocation.getArgument(0)));
        //when
        ResultActions response = mockMvc.perform(
                post(URI_PRODUCT_V1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(validProduct))
        );
        //then
        response.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(validProduct.getName())))
                .andExpect(jsonPath("$.weight", is(validProduct.getWeight())))
        ;

    }

    @Test
    void getProducts() throws Exception {
        //given
        List<ProductDto> productDtos = new ArrayList<>();
        productDtos.add(validProduct);
        productDtos.add(
                ProductDto.builder()
                        .name("product 2")
                        .weight(3)
                        .build()
        );
        given(productService.getProducts()).willReturn(productDtos);
        //when
        ResultActions response = mockMvc.perform(get(URI_PRODUCT_V1));
        //then
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(
                        jsonPath("$.size()", is(productDtos.size()))
                );
    }

    @Test
    void getProductById() throws Exception {
        //given
        given(productService.getProductById(1L)).willReturn(validProduct);
        //when
        ResultActions response = mockMvc.perform(get(URI_PRODUCT_V1 + "1"));
        //then
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.name", is(validProduct.getName())))
                .andExpect(jsonPath("$.weight", is(validProduct.getWeight())));

    }
}