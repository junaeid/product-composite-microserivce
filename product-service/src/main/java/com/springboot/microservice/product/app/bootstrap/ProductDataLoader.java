package com.springboot.microservice.product.app.bootstrap;

import com.springboot.microservice.product.app.model.Product;
import com.springboot.microservice.product.app.repository.ProductRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ProductDataLoader implements CommandLineRunner {

    private final ProductRepository productRepository;

    public ProductDataLoader(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        Product product1 = Product.builder()
                .name("product 1")
                .weight(2)
                .build();
        productRepository.save(product1);

        Product product2 = Product.builder()
                .name("product 2")
                .weight(2)
                .build();

        productRepository.save(product2);


    }
}
