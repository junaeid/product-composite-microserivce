package com.springboot.microservice.product.app.service;

import com.springboot.microservice.product.app.mappers.ProductMapper;
import com.springboot.microservice.product.app.model.Product;
import com.springboot.microservice.product.app.model.dto.ProductDto;
import com.springboot.microservice.product.app.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public ProductServiceImpl(
            ProductRepository productRepository,
            ProductMapper productMapper
    ) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }


    public List<ProductDto> getProducts() {
        return productMapper.productToProductDtos(productRepository.findAll());
    }

    public ProductDto getProductById(Long id) {
        return productMapper.productToProductDto(productRepository.findById(id).get());
    }

    public ProductDto addProduct(ProductDto productDto) {
        Product savedProduct = productRepository.save(productMapper.productDtoToProduct(productDto));
        return productMapper.productToProductDto(savedProduct);
    }
}
