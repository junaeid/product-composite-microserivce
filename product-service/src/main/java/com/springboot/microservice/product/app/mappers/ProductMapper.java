package com.springboot.microservice.product.app.mappers;

import com.springboot.microservice.product.app.model.Product;
import com.springboot.microservice.product.app.model.dto.ProductDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    List<ProductDto> productToProductDtos(List<Product> products);

    List<Product> productDtoT0Products(List<ProductDto> productDtos);

    ProductDto productToProductDto(Product product);

    Product productDtoToProduct(ProductDto productDto);
}
